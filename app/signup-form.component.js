System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SignupFormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SignupFormComponent = (function () {
                function SignupFormComponent() {
                    this.email = '';
                    this.password = '';
                }
                SignupFormComponent.prototype.onSubmit = function (form) {
                    console.log('Email: ', this.email + '  Hasło: ' + this.password);
                    form.resetForm();
                };
                SignupFormComponent = __decorate([
                    core_1.Component({
                        selector: 'signup-form',
                        template: "\n   <form  (submit)=\"onSubmit(form)\" novalidate #form=\"ngForm\">\n    <div class=\"form group\">\n      <label>Email</label>\n      <input type=\"email\" class=\"form control\" [(ngModel)]=\"email\" name=\"email\" #emailField=\"ngModel\" \n             required pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\n      <p *ngIf=\"emailField.invalid && emailField.touched && emailField.errors.required\" \n          class=\"alert alert-danger\">Wymagany jest adres e-mail</p>\n      <p *ngIf=\"emailField.invalid && emailField.touched && emailField.errors.pattern\" \n          class=\"alert alert-danger\">Adres e-mail jest nieprawid\u0142owy</p>\n    </div>\n\n    <div class=\"form group\">\n    <label>Haslo</label>\n    <input type=\"password\" class=\"form control\" [(ngModel)]=\"password\" name=\"password\" #passwordField=\"ngModel\" \n           required>\n    <p *ngIf=\"passwordField.invalid && passwordField.touched\" \n        class=\"alert alert-danger\">Wymagane jest has\u0142o</p>\n  </div>\n\n    <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"form.invalid\">Rejestracja</button>\n   </form>\n  ",
                        styles: ["\n      input.ng-touched.ng-invalid {border:solid red 2px}\n      input.ng-touched.ng-valid {border:solid green 2px}\n  "]
                    }), 
                    __metadata('design:paramtypes', [])
                ], SignupFormComponent);
                return SignupFormComponent;
            }());
            exports_1("SignupFormComponent", SignupFormComponent);
        }
    }
});
