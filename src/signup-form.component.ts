import { Component } from '@angular/core';
import { NgModel } from '@angular/forms';


@Component({
  selector: 'signup-form',
  template: `
   <form  (submit)="onSubmit(form)" novalidate #form="ngForm">
    <div class="form group">
      <label>Email</label>
      <input type="email" class="form control" [(ngModel)]="email" name="email" #emailField="ngModel" 
             required pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})">
      <p *ngIf="emailField.invalid && emailField.touched && emailField.errors.required" 
          class="alert alert-danger">Wymagany jest adres e-mail</p>
      <p *ngIf="emailField.invalid && emailField.touched && emailField.errors.pattern" 
          class="alert alert-danger">Adres e-mail jest nieprawidłowy</p>
    </div>

    <div class="form group">
    <label>Haslo</label>
    <input type="password" class="form control" [(ngModel)]="password" name="password" #passwordField="ngModel" 
           required>
    <p *ngIf="passwordField.invalid && passwordField.touched" 
        class="alert alert-danger">Wymagane jest hasło</p>
  </div>

    <button type="submit" class="btn btn-primary" [disabled]="form.invalid">Rejestracja</button>
   </form>
  `,
  styles:[`
      input.ng-touched.ng-invalid {border:solid red 2px}
      input.ng-touched.ng-valid {border:solid green 2px}
  `]
})
export class SignupFormComponent {

  email = '';
  password = '';
  
  onSubmit(form){
    console.log('Email: ', this.email +'  Hasło: '+ this.password);
    form.resetForm();
  }
}
